package com.javacoder.sso.config;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.javacoder.sso.model.ErrorResponse;

@RestControllerAdvice
public class ApiExceptionHandler {
	
	@ExceptionHandler(value = Exception.class)
    public ResponseEntity<ErrorResponse> handleGenericNotFoundException(Exception e) {
        ErrorResponse error = new ErrorResponse(
        		HttpStatus.FORBIDDEN.name(), 
        		e.getMessage(),
        		e.getMessage(),
        		new Date()
        		);
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }   

}
