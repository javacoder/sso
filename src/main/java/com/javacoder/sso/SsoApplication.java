package com.javacoder.sso;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.javacoder.sso.model.Book;

import lombok.extern.log4j.Log4j2;

@SpringBootApplication
@RestController
@Log4j2
public class SsoApplication {

	@GetMapping(path = "/books")
	public ResponseEntity<List<Book>> getAllBook() {
		log.debug("getAllBook :: {}, :: {}", "test1", "test2");
		List<Book> books = Stream.of(new Book("101", "Java", 1000d),
							  new Book("201", "NodeJS", 2000d),
							  new Book("201", "Python", 3000d))
								.collect(Collectors.toList());
		return new ResponseEntity<List<Book>>(books, HttpStatus.OK);
	}
	
	@GetMapping(path = "/logout")
    public String logout(HttpServletRequest request) throws ServletException {
		request.logout();
		return "redirect:/";
    }
	
	public static void main(String[] args) {
		SpringApplication.run(SsoApplication.class, args);
	}

}
